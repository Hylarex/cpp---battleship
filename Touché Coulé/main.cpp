#include <iut_tlse3\iutupdate.hpp>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <conio.h>

using namespace std;

const int NMAX=10;

/******** TYPES ********/

typedef struct cases
                    {
                     bool touche;
                     bool bateau;
                     char type_bateau;
                    };

typedef cases grille[NMAX][NMAX];

/******** MACROS ********/

void initialisation (grille & g_j1,grille & g_j2,bool mode);
void menu (int & score1,int & score2);
void regles (int & score1,int & score2);
void plateau (int left,int top);
void affiche_bateau (int x,int y,bool touche);
void affiche_grille_def (grille const grille);
void affiche_grille_att (grille const grille_autre_j);
char cursor ();
void placement_bateau (grille & grille,char type_bateau,int lg_bateau);
void placement_bateau_ia (grille & grille,char type_bateau,int lg_bateau);
bool presence_bateau (grille const grille,int lg_bateau,bool horiz,int x,int y);
bool bateau_coule (grille const grille,char type_bateau);
int nb_cases_touchees (grille const grille,char type_bateau);
void nb_cases_touchees_total (grille const g_j1,grille const g_j2,int & score1,int & score2);
void tir (grille const grille_j,grille & grille_autre_j,int & score1,int & score2,int num_joueur);
void tir_ia_lvl_1 (grille const g_ia,grille & g_j1,int & score1,int & score2);
void tir_ia_lvl_2 (grille const g_ia,grille & g_j1,int & score1,int & score2,bool & tir_precedent,bool & bateau_precedent_coule,int & a,int & z);
void jcj (int & score1,int & score2);
void jcm (int & score1,int & score2);

/******** SS-PROG ********/

void initialisation (grille & g_j1,grille & g_j2,bool mode)
    {
     int i,j;

     srand(time(NULL));
     system("cls");

     for(i=0;i<NMAX;i++)
        {
         for(j=0;j<NMAX;j++)
            {
             g_j1[i][j].touche=false;
             g_j1[i][j].bateau=false;
             g_j2[i][j].touche=false;
             g_j2[i][j].bateau=false;
            }
        }

     textbackground(BLACK);
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\tPLACEMENT DES BATEAUX"<<endl<<endl<<endl<<endl;
     textcolor(LIGHTGREEN);
     cout<<"\t\t\t\t      Joueur 1, a vous de placer vos bateaux !"<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t\t\t\t         Tapez sur une touche pour continuer ";
     waitkeypressed();

     plateau(42,10);
     placement_bateau(g_j1,'p',5);
     placement_bateau(g_j1,'c',4);
     placement_bateau(g_j1,'s',3);
     placement_bateau(g_j1,'r',3);
     placement_bateau(g_j1,'t',2);
     delay(1000);

     if(mode==true)
       {
        textbackground(BLACK);
        system("cls");
        textcolor(LIGHTRED);
        cout<<endl<<"\t\t\t\t\t\tPLACEMENT DES BATEAUX"<<endl<<endl<<endl<<endl;
        textcolor(LIGHTGREEN);
        cout<<"\t\t\t\t      Joueur 2, a vous de placer vos bateaux !"<<endl<<endl;
        textcolor(WHITE);
        cout<<"\t\t\t\t         Tapez sur une touche pour continuer ";
        waitkeypressed();

        plateau(42,10);
        placement_bateau(g_j2,'p',5);
        placement_bateau(g_j2,'c',4);
        placement_bateau(g_j2,'s',3);
        placement_bateau(g_j2,'r',3);
        placement_bateau(g_j2,'t',2);
        delay(1000);
       }
     else
       {
        placement_bateau_ia(g_j2,'p',5);
        placement_bateau_ia(g_j2,'c',4);
        placement_bateau_ia(g_j2,'s',3);
        placement_bateau_ia(g_j2,'r',3);
        placement_bateau_ia(g_j2,'t',2);
       }
    }

void menu (int & score1,int & score2)
    {
     char choix;

     _resize_screen(117,35);
     _setcursortype(_NORMALCURSOR);
     score1=0;
     score2=0;

     do
       {
        system("cls");
        textbackground(BLACK);
        textcolor(LIGHTRED);
        cout<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<"\t\t\t\t\t\t  BATAILLE NAVALE"<<endl<<endl<<endl<<endl;
        textcolor(YELLOW);
        cout<<"\t\t\t\t Menu :"<<endl<<endl;
        textcolor(WHITE);
        cout<<"\t\t\t\t     1-  R\x8agles du jeu,Commandes clavier,Affichage"<<endl;
        cout<<"\t\t\t\t     2-  Mode Joueur contre Joueur "<<endl;
        cout<<"\t\t\t\t     3-  Mode Joueur contre Machine "<<endl<<endl;
        textcolor(LIGHTGREEN);
        cout<<"\t\t\t\t Tapez votre choix puis appuyez sur ENTRER : ";
        textcolor(LIGHTCYAN);
        cin>>choix;
       }
     while((choix!='1')&&(choix!='2')&&(choix!='3'));

     switch(choix)
           {
            case '1' : regles(score1,score2);
                       break;
            case '2' : jcj(score1,score2);
                       break;
            case '3' : jcm(score1,score2);
                       break;
           }
    }

void regles (int & score1,int & score2)
    {
     system("cls");
     _resize_screen(131,55);

     textcolor(LIGHTRED);
     cout<<endl<<"\tBATAILLE NAVALE"<<endl<<endl<<endl<<endl;

     textcolor(YELLOW);
     cout<<"  Regles :"<<endl<<endl;
     textcolor(WHITE);
     cout<<"     Le but du jeu est d'\x88tre le premier a avoir coul\x82 tous les bateaux de son adversaire"<<endl
         <<"     Avant de commencer une partie, chaque joueur positionne ses bateaux sur sa grille dite de defense"<<endl
         <<"     Chaque joueur tire chacun son tour sur la case de son choix"<<endl
         <<"     La partie se termine lorsqu'un joueur a vu tous ses bateaux se faire couler"<<endl<<endl<<endl<<endl;
     textcolor(YELLOW);
     cout<<"  Les diff\x82rents bateaux et leur tailles :"<<endl<<endl;
     textcolor(LIGHTGRAY);
     cout<<"     Le porte-avions      ";textcolor(WHITE);cout<<": 5 cases"<<endl;
     textcolor(LIGHTGRAY);
     cout<<"     Le croiseur          ";textcolor(WHITE);cout<<": 4 cases"<<endl;
     textcolor(LIGHTGRAY);
     cout<<"     Le contre-torpilleur ";textcolor(WHITE);cout<<": 3 cases"<<endl;
     textcolor(LIGHTGRAY);
     cout<<"     Le sous-marin        ";textcolor(WHITE);cout<<": 3 cases"<<endl;
     textcolor(LIGHTGRAY);
     cout<<"     Le torpilleur        ";textcolor(WHITE);cout<<": 2 cases"<<endl<<endl<<endl<<endl;

     textcolor(YELLOW);
     cout<<"  Commandes et Affichage :"<<endl<<endl;
     textcolor(WHITE);
     cout<<"     - Les d\x82placements se font au moyen des fl"<<"\x88"<<"ches directionnelles"<<endl<<endl
         <<"     - ";textcolor(BROWN);cout<<"'t'";textcolor(WHITE);cout<<" : Tirer sur la case sur laquelle se trouve le curseur (Uniquement lorsque l'on manipule notre grille d'attaque)"<<endl<<endl
         <<"     - ";textcolor(BROWN);cout<<"'h'";textcolor(WHITE);cout<<" : Positionne le bateau a l'horizontale vers la droite, en partant de la case sur laquelle se trouve le curseur "<<endl
         <<"             (Uniquement lorque l'on manipule notre grille de defense, un bip sonore est \x82mit si le positionnement est incorrect)"<<endl
         <<"     - ";textcolor(BROWN);cout<<"'v'";textcolor(WHITE);cout<<" : Positionne le bateau a la verticale vers le bas, en partant de la case sur laquelle se trouve le curseur "<<endl
         <<"             (Uniquement lorque l'on manipule notre grille de defense, un bip sonore est \x82mit si le positionnement est incorrect)"<<endl<<endl<<endl<<endl;

     textcolor(YELLOW);
     cout<<"  Grille de defense :"<<endl<<endl;
     textcolor(WHITE);
     cout<<"     - Case";textcolor(LIGHTBLUE);cout<<" BLEUE";textcolor(WHITE);cout<<"  : Case vide"<<endl
         <<"     - Case Croix   : Case qui a \x82t\x82 cibl"<<"\x82"<<"e mais qui est vide"<<endl
         <<"     - Case";textcolor(LIGHTGREEN);cout<<" VERTE";textcolor(WHITE);cout<<"  : Pr\x82sence d'un bateau qui n'a pas \x82t\x82 touch\x82"<<endl
         <<"     - Case";textcolor(LIGHTRED);cout<<" ROUGE";textcolor(WHITE);cout<<"  : Pr\x82sence d'un bateau qui a \x82t\x82 touch\x82"<<endl<<endl;
     textcolor(YELLOW);
     cout<<"  Grille d'attaque :"<<endl<<endl;
     textcolor(WHITE);
     cout<<"     - Case";textcolor(LIGHTBLUE);cout<<" BLEUE";textcolor(WHITE);cout<<"   : Case qui n'a pas encore \x82t\x82 cibl"<<"\x82"<<"e"<<endl
         <<"     - Case Croix   : Case qui a \x82t\x82 cibl"<<"\x82"<<"e mais qui est vide"<<endl
         <<"     - Case";textcolor(LIGHTGREEN);cout<<" VERT";textcolor(WHITE);cout<<"    : Case qui a \x82t\x82 cibl"<<"\x82"<<"e et o\x97 se trouve un bateau non coul\x82"<<endl
         <<"     - Case";textcolor(LIGHTRED);cout<<" ROUGE";textcolor(WHITE);cout<<"   : Case qui a \x82t\x82 cibl"<<"\x82"<<"e et o\x97 se trouve un bateau coul\x82"<<endl<<endl<<endl<<endl;

     textcolor(LIGHTGREEN);
     cout<<"  Tapez sur une touche pour revenir au menu ";

     waitkeypressed();
     menu(score1,score2);
    }

void plateau (int left,int top)
    {
     int nbcol=11,wcol=2,nblin=11,hlin=1,i;

     textcolor(WHITE);
     textbackground(LIGHTBLUE);
     windowgrid(left,top,nbcol,wcol,nblin,hlin,THINLINE);

     for(i=0;i<NMAX;i++)
        {
         gotoxy(1,3+2*i);
         printf("%c",'A'+i);
         gotoxy(4+3*i,1);
         printf("%i",i+1);
        }

     gotoxy(4,3);
    }

void affiche_bateau (int x,int y,bool touche)
    {
     gotoxy(3*x+1,2*y+1);

     if(touche==true)
       {
        textbackground(LIGHTRED);
        printf("  ");
       }
     else
       {
        textbackground(LIGHTGREEN);
        printf("  ");
       }
     gotoxy(4,3);
    }

void affiche_grille_def (grille const grille)
    {
     int i,j;

     for(i=0;i<NMAX;i++)
        {
         for(j=0;j<NMAX;j++)
            {
             if(grille[i][j].bateau==true)
                affiche_bateau(i+1,j+1,grille[i][j].touche);
             else
               {
                if(grille[i][j].touche==true)
                  {
                   textbackground(LIGHTBLUE);
                   textcolor(WHITE);
                   gotoxy(3*(i+1)+1,2*(j+1)+1);
                   printf("><");
                  }
               }
            }
        }
    }

void affiche_grille_att (grille const grille_autre_j)
    {
     int i,j;

     for(i=0;i<NMAX;i++)
        {
         for(j=0;j<NMAX;j++)
            {
             if(grille_autre_j[i][j].touche==true)
               {
                if(grille_autre_j[i][j].bateau==true)
                  {
                   if(bateau_coule(grille_autre_j,grille_autre_j[i][j].type_bateau)==true)
                      affiche_bateau(i+1,j+1,true);
                   else
                      affiche_bateau(i+1,j+1,false);
                  }
                else
                  {
                   textbackground(LIGHTBLUE);
                   textcolor(WHITE);
                   gotoxy(3*(i+1)+1,2*(j+1)+1);
                   printf("><");
                  }
               }
            }
        }
     gotoxy(4,3);
    }

char cursor ()
    {
     int touche,x,y;

     x=(wherex()-1)/3;
     y=(wherey()-1)/2;

     touche = getxkey();

     switch (touche)
           {
            case K_EUp : y=(y+8)%10+1;
                         break;

            case K_EDown : y=y%10+1;
                           break;

            case K_ELeft : x=(x+8)%10+1;
                           break;

            case K_ERight : x=x%10+1;
                            break;
           }

     gotoxy(3*x+1,2*y+1);

     return(touche);
    }

void placement_bateau (grille & grille,char type_bateau,int lg_bateau)
    {
     int i,x,y;
     char touche;
     bool horiz=true,valide;

     do
       {
        valide=true;

        do
          {
           touche=cursor();
          }
        while((touche!='h')&&(touche!='v'));

        x=(wherex()-1)/3;
        y=(wherey()-1)/2;

        switch(touche)
              {
               case 'h' : if(x<=(11-lg_bateau))
                            {
                             if(presence_bateau(grille,lg_bateau,horiz,x,y)==false)
                               {
                                for(i=0;i<lg_bateau;i++)
                                   {
                                    affiche_bateau(x+i,y,false);
                                    grille[x+i-1][y-1].bateau=true;
                                    grille[x+i-1][y-1].type_bateau=type_bateau;
                                   }
                                gotoxy(3*(x+i)+1,2*y+1);
                               }
                             else
                               {
                                putch('\a');
                                valide=false;
                               }
                            }
                          else
                            {
                             putch('\a');
                             valide=false;
                            }
                          break;

               case 'v' : if(y<=(11-lg_bateau))
                            {
                             horiz=false;
                             if(presence_bateau(grille,lg_bateau,horiz,x,y)==false)
                               {
                                for(i=0;i<lg_bateau;i++)
                                   {
                                    affiche_bateau(x,y+i,false);
                                    grille[x-1][y+i-1].bateau=true;
                                    grille[x-1][y+i-1].type_bateau=type_bateau;
                                   }
                                gotoxy(3*x+1,2*(y+i)+1);
                               }
                             else
                               {
                                putch('\a');
                                valide=false;
                               }
                            }
                          else
                            {
                             putch('\a');
                             valide=false;
                            }
                          break;
              }
       }
     while(valide!=true);
     gotoxy(4,3);
    }

void placement_bateau_ia (grille & grille,char type_bateau,int lg_bateau)
    {
     int i,x,y;
     char touche;
     bool horiz=true,valide;

     do
       {
        valide=true;

        if(rand()%2)
           touche='h';
        else
           touche='v';

        x=rand()%10;
        y=rand()%10;

        switch(touche)
              {
               case 'h' : if(x<=(10-lg_bateau))
                            {
                             if(presence_bateau(grille,lg_bateau,horiz,x+1,y+1)==false)
                               {
                                for(i=0;i<lg_bateau;i++)
                                   {
                                    grille[x+i][y].bateau=true;
                                    grille[x+i][y].type_bateau=type_bateau;
                                   }
                               }
                             else
                                valide=false;
                            }
                          else
                             valide=false;
                          break;

               case 'v' : if(y<=(10-lg_bateau))
                            {
                             horiz=false;
                             if(presence_bateau(grille,lg_bateau,horiz,x+1,y+1)==false)
                               {
                                for(i=0;i<lg_bateau;i++)
                                   {
                                    grille[x][y+i].bateau=true;
                                    grille[x][y+i].type_bateau=type_bateau;
                                   }
                               }
                             else
                                valide=false;
                            }
                          else
                             valide=false;
                          break;
              }
       }
     while(valide!=true);
    }

bool presence_bateau (grille const grille,int lg_bateau,bool horiz,int x,int y)
    {
     int i;
     bool presence=false;

     x-=1;
     y-=1;

     if(horiz==true)
       {
        for(i=0;i<lg_bateau;i++)
           {
            if(grille[x+i][y].bateau==true)
              {
               presence=true;
              }
           }
       }
     else
       {
        for(i=0;i<lg_bateau;i++)
           {
            if(grille[x][y+i].bateau)
              {
               presence=true;
              }
           }
       }
     return(presence);
    }

bool bateau_coule (grille const grille,char type_bateau)
    {
     bool coule=false;
     int nb_cases;

     switch(type_bateau)
           {
            case 'p' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==5)
                           coule=true;
                        break;
            case 'c' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==4)
                           coule=true;
                        break;
            case 's' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==3)
                           coule=true;
                        break;
            case 'r' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==3)
                           coule=true;
                        break;
            case 't' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==2)
                           coule=true;
                        break;


           }
     return(coule);
    }

int nb_cases_touchees (grille const grille,char type_bateau)
   {
    int nb_cases=0,i,j;

    for(i=0;i<NMAX;i++)
       {
        for(j=0;j<NMAX;j++)
           {
            if((grille[i][j].type_bateau==type_bateau)&&(grille[i][j].touche==true))
               nb_cases+=1;
           }
       }
    return(nb_cases) ;
   }

void nb_cases_touchees_total (grille const g_j1,grille const g_j2,int & score1,int & score2)
    {
     int i,j;
     score1=0;
     score2=0;

     for(i=0;i<NMAX;i++)
        {
         for(j=0;j<NMAX;j++)
            {
             if((g_j2[i][j].bateau==true)&&(g_j2[i][j].touche==true))
                score1+=1;

             if((g_j1[i][j].bateau==true)&&(g_j1[i][j].touche==true))
                score2+=1;
            }
        }
    }

void tir (grille const grille_j,grille & grille_autre_j,int & score1,int & score2,int num_joueur)
    {
     int x,y;
     char touche,tir_reussi;

     _setcursortype(_NORMALCURSOR);

     textbackground(BLACK);
     system("cls");
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\tPHASE DE TIR JOUEUR "<<num_joueur<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t   GRILLE DE DEFENSE"<<"\t\t\t\t\t\t\t\tGRILLE D'ATTAQUE"<<endl<<endl<<endl<<endl
         <<"\t\t\t\t\t    Score J1 = ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<"    Score J2 = ";textcolor(YELLOW);cout<<score2;

     plateau(4,7);
     affiche_grille_def(grille_j);
     plateau(80,7);
     affiche_grille_att(grille_autre_j);

     do
       {
        touche=cursor();
        x=((wherex()-1)/3)-1;
        y=((wherey()-1)/2)-1;
       }
     while((touche!='t')||(grille_autre_j[x][y].touche==true));



     grille_autre_j[x][y].touche=true;
     tir_reussi=grille_autre_j[x][y].bateau;

     if(tir_reussi==true)
       {
        if(bateau_coule(grille_autre_j,grille_autre_j[x][y].type_bateau)==true)
          {
           putch('\a');putch('\a');putch('\a');
          }
        else
          {
           putch('\a');
           gotoxy(4,5);
          }

        if(num_joueur==1)
           nb_cases_touchees_total(grille_j,grille_autre_j,score1,score2);
        else
           nb_cases_touchees_total(grille_autre_j,grille_j,score1,score2);
       }
     affiche_grille_att(grille_autre_j);
     _setcursortype(_NOCURSOR);
     delay(500);

     if((score1<17)&&(score2<17)&&(tir_reussi==true))
        tir(grille_j,grille_autre_j,score1,score2,num_joueur);
    }

void tir_ia_lvl_1 (grille const g_ia,grille & g_j1,int & score1,int & score2)
    {
     int x,y;
     bool tir_reussi;

     _setcursortype(_NOCURSOR);

     textbackground(BLACK);
     system("cls");
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\tPHASE DE TIR DE L'IA "<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t   GRILLE DE DEFENSE"<<"\t\t\t\t\t\t\t\tGRILLE D'ATTAQUE"<<endl<<endl<<endl<<endl
         <<"\t\t\t\t\t    Score J1 = ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<"    Score J2 = ";textcolor(YELLOW);cout<<score2;

     plateau(80,7);
     affiche_grille_att(g_ia);

     do
       {
        x=rand()%10;
        y=rand()%10;
       }
     while(g_j1[x][y].touche==true);

        g_j1[x][y].touche=true;
        tir_reussi=g_j1[x][y].bateau;

        if(tir_reussi==true)
          {
           if(bateau_coule(g_j1,g_j1[x][y].type_bateau)==true)
             {
              putch('\a');putch('\a');putch('\a');
             }
           else
             {
              putch('\a');
             }
              nb_cases_touchees_total(g_j1,g_ia,score1,score2);
          }
        plateau(4,7);
        affiche_grille_def(g_j1);
        delay(2000);

     if((score1<17)&&(score2<17)&&(tir_reussi==true))
        tir_ia_lvl_1(g_ia,g_j1,score1,score2);
    }

void tir_ia_lvl_2 (grille const g_ia,grille & g_j1,int & score1,int & score2,bool & tir_precedent,bool & bateau_precedent_coule,int & a,int & z)
    {
     int x,y,autour,i=0,j=0,k=0,l=0;
     bool tir_reussi,tir_impossible=true;

     _setcursortype(_NOCURSOR);

     textbackground(BLACK);
     system("cls");
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\tPHASE DE TIR DE L'IA "<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t   GRILLE DE DEFENSE"<<"\t\t\t\t\t\t\t\tGRILLE D'ATTAQUE"<<endl<<endl<<endl<<endl
         <<"\t\t\t\t\t    Score J1 = ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<"    Score J2 = ";textcolor(YELLOW);cout<<score2;

     plateau(80,7);
     affiche_grille_att(g_ia);

     do
       {
        x=rand()%10;
        y=rand()%10;
       }
     while(g_j1[x][y].touche==true);

     if((tir_precedent==true)&&(bateau_precedent_coule==false))
       {
        do
          {
           autour=rand()%4;

           switch(autour)
                 {
                  case 0 : if((a-1)>=0)
                             {
                              x=a-1;
                              y=z;
                              tir_impossible=g_j1[x][y].touche;
                             }
                           i++;
                           break;
                  case 1 : if((z-1)>=0)
                             {
                              x=a;
                              y=z-1;
                              tir_impossible=g_j1[x][y].touche;
                             }
                           j++;
                           break;
                  case 2 : if((a+1)<=9)
                             {
                              x=a+1;
                              y=z;
                              tir_impossible=g_j1[x][y].touche;
                             }
                           k++;
                           break;
                  case 3 : if((z+1)<=9)
                             {
                              x=a;
                              y=z+1;
                              tir_impossible=g_j1[x][y].touche;
                             }
                           l++;
                           break;
                 }
          }
        while((tir_impossible==true)&&((i<1)||(j<1)||(k<1)||(l<1)));
       }

     if((i>0)&&(j>0)&&(k>0)&&(l>0))
          {
           do
             {
              x=rand()%10;
              y=rand()%10;
             }
           while(g_j1[x][y].touche==true);
          }

        g_j1[x][y].touche=true;
        tir_reussi=g_j1[x][y].bateau;

        if(tir_reussi==true)
          {

           bateau_precedent_coule=bateau_coule(g_j1,g_j1[x][y].type_bateau);
           if(bateau_precedent_coule==true)
             {
              putch('\a');putch('\a');putch('\a');
             }
           else
             {
              putch('\a');
             }
              nb_cases_touchees_total(g_j1,g_ia,score1,score2);
          }
        tir_precedent=tir_reussi;
        plateau(4,7);
        affiche_grille_def(g_j1);
        delay(2000);

     if((score1<17)&&(score2<17)&&(tir_reussi==true))
        tir_ia_lvl_2(g_ia,g_j1,score1,score2,tir_precedent,bateau_precedent_coule,x,y);
    }

void jcj (int & score1,int & score2)
    {
     int num_joueur;
     grille g_j1,g_j2;

     initialisation (g_j1,g_j2,1);

     do
      {
       num_joueur=1;
       textbackground(BLACK);
       system("cls");
       textcolor(LIGHTRED);
       cout<<endl<<"\t\t\t\t\t\tPLACEMENT DES BATEAUX"<<endl<<endl<<endl<<endl;
       textcolor(LIGHTGREEN);
       cout<<"\t\t\t\t\t      Joueur "<<num_joueur<<" a vous de tirer !"<<endl<<endl;
       textcolor(WHITE);
       cout<<"\t\t\t\t         Tapez sur une touche pour continuer ";
       waitkeypressed();
       tir(g_j1,g_j2,score1,score2,num_joueur);

       if(score1<17)
         {
          num_joueur=2;
          textbackground(BLACK);
          system("cls");
          textcolor(LIGHTRED);
          cout<<endl<<"\t\t\t\t\t\tPLACEMENT DES BATEAUX"<<endl<<endl<<endl<<endl;
          textcolor(LIGHTGREEN);
          cout<<"\t\t\t\t\t      Joueur "<<num_joueur<<" a vous de tirer !"<<endl<<endl;
          textcolor(WHITE);
          cout<<"\t\t\t\t         Tapez sur une touche pour continuer ";
          waitkeypressed();
          tir(g_j2,g_j1,score1,score2,num_joueur);
         }
      }
     while((score1<17)&&(score2<17));
     //delay(2000);
    }

void jcm (int & score1,int & score2)
    {
     grille g_j1,g_j2;
     int a,z;
     int lvl;
     bool bateau_precedent_coule=false,tir_precedent=false;

     initialisation (g_j1,g_j2,false);
     do
       {
        textbackground(BLACK);
        system("cls");
        textcolor(LIGHTRED);
        cout<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<"\t\t\t\t\t\tCHOIX DU NIVEAU DE L'IA "<<endl<<endl<<endl<<endl<<endl;
        textcolor(YELLOW);cout<<"\t\t    Niveau 1";textcolor(WHITE);cout<<" : Tir al"<<"\x82"<<"atoire"<<endl;
        textcolor(YELLOW);cout<<endl<<"\t\t    Niveau 2";textcolor(WHITE);cout<<" : Tir al"<<"\x82"<<"atoire avec tir adjacent si tir pr"<<"\x82"<<"cedent r\x82ussi";
        textcolor(LIGHTGREEN);
        cout<<endl<<endl<<endl<<"\t\t    Tapez votre choix puis appuyez sur ENTRER : ";
        textcolor(LIGHTCYAN);
        cin>>lvl;
       }
     while((lvl!=1)&&(lvl!=2));

     do
      {
       tir(g_j1,g_j2,score1,score2,1);

       if(score1<17)
         {
          switch(lvl)
                {
                 case 1 : tir_ia_lvl_1(g_j2,g_j1,score1,score2);
                            break;
                 case 2 : tir_ia_lvl_2(g_j2,g_j1,score1,score2,tir_precedent,bateau_precedent_coule,a,z);
                            break;
                }
         }
      }
     while((score1<17)&&(score2<17));
    }


/******** MAIN ********/

int main ()
   {
    int score1,score2;

    do
      {
       menu(score1,score2);

       textbackground(BLACK);
       system("cls");
       textcolor(LIGHTRED);
       cout<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<"\t\t\t\t\t\t  FIN DE PARTIE "<<endl<<endl<<endl<<endl<<endl;
       textcolor(WHITE);
       if(score1==17)
         {
          cout<<"\t\t    Le Joueur 1 gagne avec un score de ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<" contre le Joueur 2 avec un score de ";textcolor(YELLOW);cout<<score2;
         }
       else
         {
          cout<<"\t\t    Le Joueur 2 gagne avec un score de ";textcolor(YELLOW);cout<<score2;textcolor(WHITE);cout<<" contre le Joueur 1 avec un score de ";textcolor(YELLOW);cout<<score1;
         }

       textcolor(LIGHTGREEN);
       cout<<endl<<endl<<endl<<"\t\t\t\t    Tapez sur une touche pour revenir au menu ";

       waitkeypressed();
      }
    while(1);

    return (0);
   }

