#include <iut_tlse3\iutupdate.hpp>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <conio.h>
#include <vector>

using namespace std;

const int NMAX=10;

/******** TYPES ********/

typedef struct cases
                    {
                     bool touche;
                     bool bateau;
                     char type_bateau;
                    };

typedef struct coordonnees
                          {
                           int x;
                           int y;
                          };


typedef cases grille[NMAX][NMAX];

/******** MACROS ********/

/***47*/ bool menu (int & score1,int & score2, bool & abandon);
/**126*/ void regles (int & score1,int & score2);
/**191*/ void initialisation (grille & g_j1,grille & g_j2,bool mode);
/**275*/ void plateau (int left,int top);
/**294*/ void placement_bateau (grille & grille,char type_bateau,int lg_bateau);
/**371*/ void placement_bateau_ia (grille & grille,char type_bateau,int lg_bateau);
/**430*/ bool presence_bateau (grille const grille,int lg_bateau,bool horiz,int x,int y);
/**461*/ void affiche_bateau (int x,int y,bool touche);
/**478*/ void affiche_grille_def (grille const grille);
/**502*/ void affiche_grille_att (grille const grille_autre_j);
/**532*/ char cursor ();
/**560*/ int  nb_cases_touchees (grille const grille,char type_bateau);
/**575*/ bool bateau_coule (grille const grille,char type_bateau);
/**606*/ void nb_cases_touchees_total (grille const g_j1,grille const g_j2,int & score1,int & score2);
/**626*/ void jcj (int & score1,int & score2,bool & abandon);
/**667*/ void tir (grille const grille_j,grille & grille_autre_j,int & score1,int & score2,int num_joueur,bool ia,bool & abandon);
/**791*/ void jcm (int & score1,int & score2,bool & abandon);
/**869*/ void tir_ia_lvl_1 (grille const g_ia,grille & g_j1,int & score1,int & score2);
/**916*/ void tir_ia_lvl_2 (grille const g_ia,grille & g_j1,int & score1,int & score2, bool & tir_precedent,int & last_x, int & last_y,bool & last_ship_sank);
/*1021*/ void tir_ia_lvl_3 ();

/******** SS-PROG ********/

bool menu (int & score1,int & score2,bool & abandon)
    {
     int choix=0,touche;
     bool ia=false;

     system("cls");
     _resize_screen(117,35);
     _setcursortype(_NOCURSOR);
     score1=0;
     score2=0;

     do
       {
        textbackground(BLACK);
        system("cls");
        textcolor(LIGHTRED);
        cout<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<"\t\t\t\t\t\t   BATAILLE NAVALE"<<endl<<endl<<endl<<endl;
        textcolor(YELLOW);
        cout<<"\t\t\t\t   Menu :"<<endl<<endl<<endl;
        textcolor(WHITE);

        switch(choix)
              {
               case 0 :   cout<<"\t\t\t\t   1- ";
                          textbackground(LIGHTBLUE);
                          cout<<"R\x8agles du jeu, Commandes clavier, Affichage"<<endl<<endl;
                          textbackground(BLACK);
                          cout<<"\t\t\t\t   2- Mode Joueur contre Joueur "<<endl<<endl;
                          cout<<"\t\t\t\t   3- Mode Joueur contre Machine "<<endl<<endl<<endl;
                          break;

               case 1 :   cout<<"\t\t\t\t   1- R\x8agles du jeu, Commandes clavier, Affichage"<<endl<<endl;
                          cout<<"\t\t\t\t   2- ";
                          textbackground(LIGHTBLUE);
                          cout<<"Mode Joueur contre Joueur "<<endl<<endl;
                          textbackground(BLACK);
                          cout<<"\t\t\t\t   3- Mode Joueur contre Machine "<<endl<<endl<<endl;
                          break;

               case 2 :   cout<<"\t\t\t\t   1- R\x8agles du jeu, Commandes clavier, Affichage"<<endl<<endl;
                          cout<<"\t\t\t\t   2- Mode Joueur contre Joueur "<<endl<<endl;
                          cout<<"\t\t\t\t   3- ";
                          textbackground(LIGHTBLUE);
                          cout<<"Mode Joueur contre Machine "<<endl<<endl<<endl;
                          textbackground(BLACK);
                          break;
              }

        textcolor(LIGHTGREEN);
        cout<<"\t\t\t\t  Selectionnez votre choix puis appuyez sur ENTREE ";

        touche = getxkey();

        switch (touche)
              {
               case K_EUp   : choix=(choix+2)%3;
                              break;
               case K_EDown : choix=(choix+1)%3;
                              break;
              }
       }
     while(touche != 13);

     switch(choix)
           {
            case 0 : regles(score1,score2);
                       break;
            case 1 : jcj(score1,score2,abandon);
                       break;
            case 2 : jcm(score1,score2,abandon);
                       break;
           }

     if(choix==2)
       ia=true;

     return(ia);
    }

void regles (int & score1,int & score2)
    {
     bool abandon=false;

     system("cls");
     _resize_screen(131,60);

     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\t\tLES REGLES"<<endl<<endl<<endl<<endl;

     textcolor(YELLOW);
     cout<<"  Objectif :"<<endl<<endl;
     textcolor(WHITE);
     cout<<"     Le but du jeu est d'\x88tre le premier a avoir coul\x82 tous les bateaux de son adversaire"<<endl<<endl
         <<"     Avant de commencer une partie, chaque joueur positionne ses bateaux sur sa grille dite de defense"<<endl
         <<"     Chacun leur tour, les joueurs tirent sur la case de leur choix"<<endl<<endl
         <<"     La partie se termine lorsqu'un joueur a vu tous ses bateaux se faire couler"<<endl<<endl<<endl;

     textcolor(YELLOW);
     cout<<"  Les diff\x82rents bateaux et leurs tailles :"<<endl<<endl;
     textcolor(DARKGRAY);
     cout<<"     Le porte-avions      ";textcolor(WHITE);cout<<": 5 cases"<<endl;
     textcolor(DARKGRAY);
     cout<<"     Le croiseur          ";textcolor(WHITE);cout<<": 4 cases"<<endl;
     textcolor(DARKGRAY);
     cout<<"     Le contre-torpilleur ";textcolor(WHITE);cout<<": 3 cases"<<endl;
     textcolor(DARKGRAY);
     cout<<"     Le sous-marin        ";textcolor(WHITE);cout<<": 3 cases"<<endl;
     textcolor(DARKGRAY);
     cout<<"     Le torpilleur        ";textcolor(WHITE);cout<<": 2 cases"<<endl<<endl<<endl;

     textcolor(YELLOW);
     cout<<"  Commandes:"<<endl<<endl;
     textcolor(WHITE);
     cout<<"     - Les d\x82placements se font au moyen des fl"<<"\x88"<<"ches directionnelles"<<endl<<endl
         <<"     - ";textcolor(LIGHTMAGENTA);cout<<"'h'";textcolor(WHITE);cout<<" : Positionne le bateau a l'horizontale vers la droite, en partant de la case sur laquelle se trouve le curseur "<<endl
         <<"             (Uniquement lorque l'on manipule notre grille de defense, un bip sonore est \x82mit si le positionnement est incorrect)"<<endl<<endl
         <<"     - ";textcolor(LIGHTMAGENTA);cout<<"'v'";textcolor(WHITE);cout<<" : Positionne le bateau a la verticale vers le bas, en partant de la case sur laquelle se trouve le curseur "<<endl
         <<"             (Uniquement lorque l'on manipule notre grille de defense, un bip sonore est \x82mit si le positionnement est incorrect)"<<endl<<endl<<endl
         <<"     - ";textcolor(LIGHTMAGENTA);cout<<"'Entree'";textcolor(WHITE);cout<<" : Tirer sur la case sur laquelle se trouve le curseur (Uniquement lorsque l'on manipule notre grille d'attaque)"<<endl<<endl
         <<"     - ";textcolor(LIGHTMAGENTA);cout<<"'Echap'";textcolor(WHITE);cout<<"  : Abandonner la partie en cours (Uniquement lorsque l'on manipule notre grille d'attaque)"<<endl<<endl<<endl;

     textcolor(YELLOW);
     cout<<"  Affichage :"<<endl<<endl;
     cout<<"   Grille de defense :"<<endl<<endl;
     textcolor(WHITE);
     cout<<"     - Case";textcolor(LIGHTBLUE);cout<<" BLEUE";textcolor(WHITE);cout<<"  : Case vide"<<endl
         <<"     - Case CROIX  : Case qui a \x82t\x82 cibl"<<"\x82"<<"e mais qui est vide"<<endl
         <<"     - Case";textcolor(LIGHTGREEN);cout<<" VERTE";textcolor(WHITE);cout<<"  : Pr\x82sence d'un bateau qui n'a pas \x82t\x82 touch\x82"<<endl
         <<"     - Case";textcolor(LIGHTRED);cout<<" ROUGE";textcolor(WHITE);cout<<"  : Pr\x82sence d'un bateau qui a \x82t\x82 touch\x82"<<endl<<endl;
     textcolor(YELLOW);
     cout<<"   Grille d'attaque :"<<endl<<endl;
     textcolor(WHITE);
     cout<<"     - Case";textcolor(LIGHTBLUE);cout<<" BLEUE";textcolor(WHITE);cout<<"  : Case qui n'a pas encore \x82t\x82 cibl"<<"\x82"<<"e"<<endl
         <<"     - Case CROIX  : Case qui a \x82t\x82 cibl"<<"\x82"<<"e mais qui est vide"<<endl
         <<"     - Case";textcolor(LIGHTGREEN);cout<<" VERT";textcolor(WHITE);cout<<"   : Case qui a \x82t\x82 cibl"<<"\x82"<<"e et o\x97 se trouve un bateau non coul\x82"<<endl
         <<"     - Case";textcolor(LIGHTRED);cout<<" ROUGE";textcolor(WHITE);cout<<"  : Case qui a \x82t\x82 cibl"<<"\x82"<<"e et o\x97 se trouve un bateau coul\x82"<<endl<<endl<<endl<<endl;

     textcolor(LIGHTGREEN);
     cout<<"\t\t\t\t\tTapez sur une touche pour revenir au menu ";

     waitkeypressed();
     menu(score1,score2,abandon);
    }

void initialisation (grille & g_j1,grille & g_j2,bool mode)
    {
     int i,j;

     srand(time(NULL));
     system("cls");
     _setcursortype(_NOCURSOR);

     for(i=0;i<NMAX;i++)
        {
         for(j=0;j<NMAX;j++)
            {
             g_j1[i][j].touche=false;
             g_j1[i][j].bateau=false;
             g_j2[i][j].touche=false;
             g_j2[i][j].bateau=false;
            }
        }

     textbackground(BLACK);
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\t PLACEMENT DES BATEAUX"<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t\t\t\t       Joueur 1, a vous de placer vos bateaux !"<<endl<<endl<<endl;
     textcolor(LIGHTGREEN);
     cout<<"\t\t\t\t          Tapez sur une touche pour continuer ";
     waitkeypressed();

     system("cls");
     _setcursortype(_NORMALCURSOR);
     textbackground(BLACK);
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\t PLACEMENT DES BATEAUX"<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t\t\t\t       Joueur 1, a vous de placer vos bateaux !"<<endl<<endl<<endl<<endl<<endl;

     plateau(43,10);
     placement_bateau(g_j1,'p',5);
     placement_bateau(g_j1,'c',4);
     placement_bateau(g_j1,'s',3);
     placement_bateau(g_j1,'r',3);
     placement_bateau(g_j1,'t',2);
     _setcursortype(_NOCURSOR);
     delay(1000);

     if(mode==true)
       {
        textbackground(BLACK);
        system("cls");
        textcolor(LIGHTRED);
        cout<<endl<<"\t\t\t\t\t\t PLACEMENT DES BATEAUX"<<endl<<endl<<endl<<endl;
        textcolor(WHITE);
        cout<<"\t\t\t\t       Joueur 2, a vous de placer vos bateaux !"<<endl<<endl<<endl;
        textcolor(LIGHTGREEN);
        cout<<"\t\t\t\t          Tapez sur une touche pour continuer ";
        waitkeypressed();

        system("cls");
        _setcursortype(_NORMALCURSOR);
        textbackground(BLACK);
        textcolor(LIGHTRED);
        cout<<endl<<"\t\t\t\t\t\t PLACEMENT DES BATEAUX"<<endl<<endl<<endl<<endl;
        textcolor(WHITE);
        cout<<"\t\t\t\t       Joueur 2, a vous de placer vos bateaux !"<<endl<<endl<<endl<<endl<<endl;

        plateau(43,10);
        placement_bateau(g_j2,'p',5);
        placement_bateau(g_j2,'c',4);
        placement_bateau(g_j2,'s',3);
        placement_bateau(g_j2,'r',3);
        placement_bateau(g_j2,'t',2);
        _setcursortype(_NOCURSOR);
        delay(1000);
       }
     else
       {
        placement_bateau_ia(g_j2,'p',5);
        placement_bateau_ia(g_j2,'c',4);
        placement_bateau_ia(g_j2,'s',3);
        placement_bateau_ia(g_j2,'r',3);
        placement_bateau_ia(g_j2,'t',2);
       }
    }

void plateau (int left,int top)
    {
     int nbcol=11,wcol=2,nblin=11,hlin=1,i;

     textcolor(WHITE);
     textbackground(LIGHTBLUE);
     windowgrid(left,top,nbcol,wcol,nblin,hlin,THINLINE);

     for(i=0;i<NMAX;i++)
        {
         gotoxy(1,3+2*i);
         printf("%c",'A'+i);
         gotoxy(4+3*i,1);
         printf("%i",i+1);
        }

     gotoxy(4,3);
    }

void placement_bateau (grille & grille,char type_bateau,int lg_bateau)
    {
     int i,x,y;
     char touche;
     bool horiz=true,valide;

     do
       {
        valide=true;

        do
          {
           touche=cursor();
          }
        while((touche!='h')&&(touche!='v'));

        x=(wherex()-1)/3;
        y=(wherey()-1)/2;

        switch(touche)
              {
               case 'h' : if(x<=(11-lg_bateau))
                            {
                             if(presence_bateau(grille,lg_bateau,horiz,x,y)==false)
                               {
                                for(i=0;i<lg_bateau;i++)
                                   {
                                    affiche_bateau(x+i,y,false);
                                    grille[x+i-1][y-1].bateau=true;
                                    grille[x+i-1][y-1].type_bateau=type_bateau;
                                   }
                                gotoxy(3*(x+i)+1,2*y+1);
                               }
                             else
                               {
                                putch('\a');
                                valide=false;
                               }
                            }
                          else
                            {
                             putch('\a');
                             valide=false;
                            }
                          break;

               case 'v' : if(y<=(11-lg_bateau))
                            {
                             horiz=false;
                             if(presence_bateau(grille,lg_bateau,horiz,x,y)==false)
                               {
                                for(i=0;i<lg_bateau;i++)
                                   {
                                    affiche_bateau(x,y+i,false);
                                    grille[x-1][y+i-1].bateau=true;
                                    grille[x-1][y+i-1].type_bateau=type_bateau;
                                   }
                                gotoxy(3*x+1,2*(y+i)+1);
                               }
                             else
                               {
                                putch('\a');
                                valide=false;
                               }
                            }
                          else
                            {
                             putch('\a');
                             valide=false;
                            }
                          break;
              }
       }
     while(valide!=true);
     gotoxy(4,3);
    }

void placement_bateau_ia (grille & grille,char type_bateau,int lg_bateau)
    {
     int i,x,y;
     char touche;
     bool horiz=true,valide;

     do
       {
        valide=true;

        if(rand()%2)
           touche='h';
        else
           touche='v';

        x=rand()%10;
        y=rand()%10;

        switch(touche)
              {
               case 'h' : if(x<=(10-lg_bateau))
                            {
                             if(presence_bateau(grille,lg_bateau,horiz,x+1,y+1)==false)
                               {
                                for(i=0;i<lg_bateau;i++)
                                   {
                                    grille[x+i][y].bateau=true;
                                    grille[x+i][y].type_bateau=type_bateau;
                                   }
                               }
                             else
                                valide=false;
                            }
                          else
                             valide=false;
                          break;

               case 'v' : if(y<=(10-lg_bateau))
                            {
                             horiz=false;
                             if(presence_bateau(grille,lg_bateau,horiz,x+1,y+1)==false)
                               {
                                for(i=0;i<lg_bateau;i++)
                                   {
                                    grille[x][y+i].bateau=true;
                                    grille[x][y+i].type_bateau=type_bateau;
                                   }
                               }
                             else
                                valide=false;
                            }
                          else
                             valide=false;
                          break;
              }
       }
     while(valide!=true);
    }

bool presence_bateau (grille const grille,int lg_bateau,bool horiz,int x,int y)
    {
     int i;
     bool presence=false;

     x-=1;
     y-=1;

     if(horiz==true)
       {
        for(i=0;i<lg_bateau;i++)
           {
            if(grille[x+i][y].bateau==true)
              {
               presence=true;
              }
           }
       }
     else
       {
        for(i=0;i<lg_bateau;i++)
           {
            if(grille[x][y+i].bateau)
              {
               presence=true;
              }
           }
       }
     return(presence);
    }

void affiche_bateau (int x,int y,bool touche)
    {
     gotoxy(3*x+1,2*y+1);

     if(touche==true)
       {
        textbackground(LIGHTRED);
        printf("  ");
       }
     else
       {
        textbackground(LIGHTGREEN);
        printf("  ");
       }
     gotoxy(4,3);
    }

void affiche_grille_def (grille const grille)
    {
     int i,j;

     for(i=0;i<NMAX;i++)
        {
         for(j=0;j<NMAX;j++)
            {
             if(grille[i][j].bateau==true)
                affiche_bateau(i+1,j+1,grille[i][j].touche);
             else
               {
                if(grille[i][j].touche==true)
                  {
                   textbackground(LIGHTBLUE);
                   textcolor(WHITE);
                   gotoxy(3*(i+1)+1,2*(j+1)+1);
                   printf("><");
                  }
               }
            }
        }
    }

void affiche_grille_att (grille const grille_autre_j)
    {
     int i,j;

     for(i=0;i<NMAX;i++)
        {
         for(j=0;j<NMAX;j++)
            {
             if(grille_autre_j[i][j].touche==true)
               {
                if(grille_autre_j[i][j].bateau==true)
                  {
                   if(bateau_coule(grille_autre_j,grille_autre_j[i][j].type_bateau)==true)
                      affiche_bateau(i+1,j+1,true);
                   else
                      affiche_bateau(i+1,j+1,false);
                  }
                else
                  {
                   textbackground(LIGHTBLUE);
                   textcolor(WHITE);
                   gotoxy(3*(i+1)+1,2*(j+1)+1);
                   printf("><");
                  }
               }
            }
        }
     gotoxy(4,3);
    }

char cursor ()
    {
     int touche,x,y;

     x=(wherex()-1)/3;
     y=(wherey()-1)/2;

     touche = getxkey();

     gotoxy(3*x+1,2*y+1);

     switch (touche)
           {
            case K_EUp    : y=(y+8)%10+1;
                            break;
            case K_EDown  : y=y%10+1;
                            break;
            case K_ELeft  : x=(x+8)%10+1;
                            break;
            case K_ERight : x=x%10+1;
                            break;
           }

     gotoxy(3*x+1,2*y+1);

     return(touche);
    }

int nb_cases_touchees (grille const grille,char type_bateau)
   {
    int nb_cases=0,i,j;

    for(i=0;i<NMAX;i++)
       {
        for(j=0;j<NMAX;j++)
           {
            if((grille[i][j].type_bateau==type_bateau)&&(grille[i][j].touche==true))
               nb_cases+=1;
           }
       }
    return(nb_cases) ;
   }

bool bateau_coule (grille const grille,char type_bateau)
    {
     bool coule=false;
     int nb_cases;

     switch(type_bateau)
           {
            case 'p' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==5)
                           coule=true;
                        break;
            case 'c' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==4)
                           coule=true;
                        break;
            case 's' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==3)
                           coule=true;
                        break;
            case 'r' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==3)
                           coule=true;
                        break;
            case 't' :  nb_cases=nb_cases_touchees(grille,type_bateau);
                        if(nb_cases==2)
                           coule=true;
                        break;
           }
     return(coule);
    }

void nb_cases_touchees_total (grille const g_j1,grille const g_j2,int & score1,int & score2)
    {
     int i,j;

     score1=0;
     score2=0;

     for(i=0;i<NMAX;i++)
        {
         for(j=0;j<NMAX;j++)
            {
             if((g_j2[i][j].bateau==true)&&(g_j2[i][j].touche==true))
                score1+=1;

             if((g_j1[i][j].bateau==true)&&(g_j1[i][j].touche==true))
                score2+=1;
            }
        }
    }

void jcj (int & score1,int & score2,bool & abandon)
    {
     int num_joueur;
     grille g_j1,g_j2;

     initialisation (g_j1,g_j2,true);

     do
       {
        num_joueur=1;
        textbackground(BLACK);
        _setcursortype(_NOCURSOR);
        system("cls");
        textcolor(LIGHTRED);
        cout<<endl<<"\t\t\t\t\t\t     PHASE DE TIR"<<endl<<endl<<endl<<endl;
        textcolor(WHITE);
        cout<<"\t\t\t\t\t      Joueur 1, a vous de tirer !"<<endl<<endl<<endl;
        textcolor(LIGHTGREEN);
        cout<<"\t\t\t\t          Tapez sur une touche pour continuer ";
        waitkeypressed();
        tir(g_j1,g_j2,score1,score2,num_joueur,false,abandon);

        if((score1<17)&&(abandon==false))
          {
           num_joueur=2;
           textbackground(BLACK);
           _setcursortype(_NOCURSOR);
           system("cls");
           textcolor(LIGHTRED);
           cout<<endl<<"\t\t\t\t\t\t     PHASE DE TIR"<<endl<<endl<<endl<<endl;
           textcolor(WHITE);
           cout<<"\t\t\t\t\t      Joueur 2, a vous de tirer !"<<endl<<endl<<endl;
           textcolor(LIGHTGREEN);
           cout<<"\t\t\t\t          Tapez sur une touche pour continuer ";
           waitkeypressed();
           tir(g_j2,g_j1,score1,score2,num_joueur,false,abandon);
          }
       }
     while((score1<17)&&(score2<17)&&(abandon==false));
    }

void tir (grille const grille_j,grille & grille_autre_j,int & score1,int & score2,int num_joueur,bool ia,bool & abandon)
    {
     int touche,x,y,verif_abandon=0;
     char tir_reussi;

     textbackground(BLACK);
     system("cls");
     _setcursortype(_NORMALCURSOR);
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\tPHASE DE TIR JOUEUR "<<num_joueur<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t   GRILLE DE DEFENSE"<<"\t\t\t\t\t\t\t\tGRILLE D'ATTAQUE"<<endl<<endl<<endl<<endl
         <<"\t\t\t\t\t    Score J1 = ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<"     Score ";
     if(ia==true)
       {
        cout<<"IA = ";textcolor(YELLOW);cout<<score2;
       }
     else
       {
        cout<<"J2 = ";textcolor(YELLOW);cout<<score2;
       }

     plateau(4,7);
     affiche_grille_def(grille_j);
     plateau(80,7);
     affiche_grille_att(grille_autre_j);

     do
       {
        touche=cursor();
        x=((wherex()-1)/3)-1;
        y=((wherey()-1)/2)-1;

        if(touche==27)
          {
           do
             {
              _setcursortype(_NOCURSOR);
              textbackground(BLACK);
              system("cls");
              textcolor(LIGHTRED);
              cout<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<"\t\t\t\t\t\t       ABANDON "<<endl<<endl<<endl<<endl<<endl;
              textcolor(YELLOW);
              cout<<"\t\t\t  Attention ! Voulez vous vraiment abandonner la partie en cours ?"<<endl<<endl;
              textcolor(WHITE);

              switch(verif_abandon)
                    {
                     case 0 : cout<<"\t\t\t\t\t\t ";
                              textbackground(LIGHTBLUE);
                              cout<<"OUI";
                              textbackground(BLACK);
                              cout<<"\t\t NON"<<endl<<endl<<endl;
                              break;
                     case 1 : cout<<"\t\t\t\t\t\t OUI\t\t ";
                              textbackground(LIGHTBLUE);
                              cout<<"NON"<<endl<<endl<<endl;
                              textbackground(BLACK);
                              break;
                    }

              textcolor(LIGHTGREEN);
              cout<<endl<<"\t\t\t\t Selectionnez votre choix puis appuyez sur ENTREE ";

              touche = getxkey();

              switch(touche)
                {
                 case K_ELeft  :
                 case K_ERight : verif_abandon=(verif_abandon+1)%2;
                                 break;
                }
             }
           while(touche!=13);

           if(verif_abandon==0)
              abandon=true;
           else
             {
              textbackground(BLACK);
              system("cls");
              _setcursortype(_NORMALCURSOR);
              textcolor(LIGHTRED);
              cout<<endl<<"\t\t\t\t\t\tPHASE DE TIR JOUEUR "<<num_joueur<<endl<<endl<<endl<<endl;
              textcolor(WHITE);
              cout<<"\t   GRILLE DE DEFENSE"<<"\t\t\t\t\t\t\t\tGRILLE D'ATTAQUE"<<endl<<endl<<endl<<endl
                  <<"\t\t\t\t\t    Score J1 = ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<"    Score J2 = ";textcolor(YELLOW);cout<<score2;

              plateau(4,7);
              affiche_grille_def(grille_j);
              plateau(80,7);
              affiche_grille_att(grille_autre_j);

              verif_abandon=0;
             }
           touche=0;
          }
       }
     while(((touche!=13)||(grille_autre_j[x][y].touche==true))&&(abandon==false));

     if(abandon==false)
       {
        grille_autre_j[x][y].touche=true;
        tir_reussi=grille_autre_j[x][y].bateau;

        if(tir_reussi==true)
         {
          if(bateau_coule(grille_autre_j,grille_autre_j[x][y].type_bateau)==true)
            {
             putch('\a');putch('\a');putch('\a');
            }
          else
            {
             putch('\a');
             gotoxy(4,5);
            }

          if(num_joueur==1)
            nb_cases_touchees_total(grille_j,grille_autre_j,score1,score2);
          else
            nb_cases_touchees_total(grille_autre_j,grille_j,score1,score2);
         }

        affiche_grille_att(grille_autre_j);
        _setcursortype(_NOCURSOR);
        delay(500);

        if((score1<17)&&(score2<17)&&(tir_reussi==true))
          tir(grille_j,grille_autre_j,score1,score2,num_joueur,ia,abandon);
       }
    }

void jcm (int & score1,int & score2,bool & abandon)
    {
     int touche,last_x,last_y,lvl=0;
     bool tir_precedent=false,last_ship_sank=false;
     grille g_j1,g_j2;

     initialisation (g_j1,g_j2,false);

     do
       {
        textbackground(BLACK);
        _setcursortype(_NOCURSOR);
        system("cls");
        textcolor(LIGHTRED);
        cout<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<"\t\t\t\t\t\tCHOIX DU NIVEAU DE L'IA "<<endl<<endl<<endl<<endl<<endl;

        switch(lvl)
              {
               case 0 :   cout<<"\t\t\t    ";
                          textbackground(LIGHTBLUE);
                          textcolor(YELLOW);cout<<"Niveau 1";textcolor(WHITE);cout<<" : Il serait plus difficile de jouer contre un mur"<<endl;
                          textbackground(BLACK);
                          textcolor(YELLOW);cout<<endl<<"\t\t\t    Niveau 2";textcolor(WHITE);cout<<" : Des "<<"\x82"<<"lans d"<<"\x27"<<"intelligence avec un enrobage de stupidit"<<"\x82"<<endl;
                          textcolor(YELLOW);cout<<endl<<"\t\t\t    Niveau 3";textcolor(WHITE);cout<<" : Une machine, une vraie"<<endl;
                          break;

               case 1 :   textcolor(YELLOW);cout<<"\t\t\t    Niveau 1";textcolor(WHITE);cout<<" : Il serait plus difficile de jouer contre un mur"<<endl;
                          cout<<endl<<"\t\t\t    ";
                          textbackground(LIGHTBLUE);
                          textcolor(YELLOW);cout<<"Niveau 2";textcolor(WHITE);cout<<" : Des "<<"\x82"<<"lans d"<<"\x27"<<"intelligence avec un enrobage de stupidit"<<"\x82"<<endl;
                          textbackground(BLACK);
                          textcolor(YELLOW);cout<<endl<<"\t\t\t    Niveau 3";textcolor(WHITE);cout<<" : Une machine, une vraie"<<endl;
                          break;

               case 2 :   textcolor(YELLOW);cout<<"\t\t\t    Niveau 1";textcolor(WHITE);cout<<" : Il serait plus difficile de jouer contre un mur"<<endl;
                          textcolor(YELLOW);cout<<endl<<"\t\t\t    "<<"Niveau 2";textcolor(WHITE);cout<<" : Des "<<"\x82"<<"lans d"<<"\x27"<<"intelligence avec un enrobage de stupidit"<<"\x82"<<endl;
                          cout<<endl<<"\t\t\t    ";
                          textbackground(LIGHTBLUE);
                          textcolor(YELLOW);cout<<"Niveau 3";textcolor(WHITE);cout<<" : Une machine, une vraie"<<endl;
                          textbackground(BLACK);
                          break;
              }

        textcolor(LIGHTGREEN);
        cout<<endl<<endl<<"\t\t\t\t Selectionnez votre choix puis appuyez sur ENTREE ";

        touche = getxkey();

        switch (touche)
              {
               case K_EUp   : lvl=(lvl+2)%3;
                              break;
               case K_EDown : lvl=(lvl+1)%3;
                              break;
              }
       }
     while(touche != 13);

     do
      {
       tir(g_j1,g_j2,score1,score2,1,true,abandon);

       if((score1<17)&&(abandon==false))
         {
          switch(lvl)
                {
                 case 0 : tir_ia_lvl_1(g_j2,g_j1,score1,score2);
                            break;
                 case 1 : tir_ia_lvl_2(g_j2,g_j1,score1,score2,tir_precedent,last_x,last_y,last_ship_sank);
                            break;
                 /*case 2 : tir_ia_lvl_3();
                            break;*/
                }
         }
      }
     while((score1<17)&&(score2<17)&&(abandon==false));
    }

void tir_ia_lvl_1 (grille const g_ia,grille & g_j1,int & score1,int & score2)
    {
     int x,y;
     bool tir_reussi;

     srand(time(NULL));

     _setcursortype(_NOCURSOR);
     textbackground(BLACK);
     system("cls");
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\tPHASE DE TIR DE L'IA "<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t   GRILLE DE DEFENSE"<<"\t\t\t\t\t\t\t\tGRILLE D'ATTAQUE"<<endl<<endl<<endl<<endl
         <<"\t\t\t\t\t    Score J1 = ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<"     Score IA = ";textcolor(YELLOW);cout<<score2;

     plateau(80,7);
     affiche_grille_att(g_ia);

     do
       {
        x=rand()%10;
        y=rand()%10;
       }
     while(g_j1[x][y].touche==true);

     g_j1[x][y].touche=true;
     tir_reussi=g_j1[x][y].bateau;

     if(tir_reussi==true)
       {
        if(bateau_coule(g_j1,g_j1[x][y].type_bateau)==true)
          {
           putch('\a');putch('\a');putch('\a');
          }
        else
           putch('\a');

        nb_cases_touchees_total(g_j1,g_ia,score1,score2);
       }

     plateau(4,7);
     affiche_grille_def(g_j1);
     delay(2000);

     if((score1<17)&&(score2<17)&&(tir_reussi==true))
        tir_ia_lvl_1(g_ia,g_j1,score1,score2);
    }

void tir_ia_lvl_2 (grille const g_ia,grille & g_j1,int & score1,int & score2, bool & tir_precedent,int & last_x, int & last_y,bool & last_ship_sank)
    {
     int x,y,r,i=0;
     bool tir_reussi;
     vector<coordonnees> posibilities;

     srand(time(NULL));

     _setcursortype(_NOCURSOR);
     textbackground(BLACK);
     system("cls");
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\tPHASE DE TIR DE L'IA "<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t   GRILLE DE DEFENSE"<<"\t\t\t\t\t\t\t\tGRILLE D'ATTAQUE"<<endl<<endl<<endl<<endl
         <<"\t\t\t\t\t    Score J1 = ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<"     Score IA = ";textcolor(YELLOW);cout<<score2;

     plateau(80,7);
     affiche_grille_att(g_ia);

     if((tir_precedent==true)&&(last_ship_sank==false))
       {
        if(g_j1[last_x-1][last_y].touche==false && (last_x-1 >= 0))
          {
           posibilities.push_back();
           posibilities[posibilities.size()-1].x = last_x-1;
           posibilities[posibilities.size()-1].y = last_y;
          }

        if(g_j1[last_x][last_y-1].touche==false && (last_y-1 >= 0))
          {
           posibilities.push_back();
           posibilities[posibilities.size()-1].x = last_x;
           posibilities[posibilities.size()-1].y = last_y-1;
          }

        if(g_j1[last_x+1][last_y].touche==false && (last_x+1 < 10))
          {
           posibilities.push_back();
           posibilities[posibilities.size()-1].x = last_x+1;
           posibilities[posibilities.size()-1].y = last_y;
          }

        if(g_j1[last_x][last_y+1].touche==false && (last_y+1 < 10))
          {
           posibilities.push_back();
           posibilities[posibilities.size()-1].x = last_x;
           posibilities[posibilities.size()-1].y = last_y+1;
          }
       }

     if(posibilities.size()!=0)
       {
        r=rand()%posibilities.size();

        x=posibilities[r].x;
        y=posibilities[r].y;
       }
     else
       {
        do
          {
           x=rand()%10;
           y=rand()%10;
          }
        while(g_j1[x][y].touche==true);
       }

     g_j1[x][y].touche=true;
     tir_reussi=g_j1[x][y].bateau;
     last_ship_sank=false;

     if(tir_reussi==true)
       {
        if(bateau_coule(g_j1,g_j1[x][y].type_bateau)==true)
          {
           putch('\a');putch('\a');putch('\a');
           last_ship_sank=true;
          }
        else
           putch('\a');

        nb_cases_touchees_total(g_j1,g_ia,score1,score2);
       }

     plateau(4,7);
     affiche_grille_def(g_j1);
     delay(2000);

///////////////////////////////////////////////////////////////////
     last_x = x;
     last_y = y;
///////////////////////////////////////////////////////////////////


     if((score1<17)&&(score2<17)&&(tir_reussi==true))
        tir_ia_lvl_2(g_ia,g_j1,score1,score2,tir_precedent,last_x,last_y,last_ship_sank);
    }

/*void tir_ia_lvl_3 ()
    {

     _setcursortype(_NOCURSOR);

     textbackground(BLACK);
     system("cls");
     textcolor(LIGHTRED);
     cout<<endl<<"\t\t\t\t\t\tPHASE DE TIR DE L'IA "<<endl<<endl<<endl<<endl;
     textcolor(WHITE);
     cout<<"\t   GRILLE DE DEFENSE"<<"\t\t\t\t\t\t\t\tGRILLE D'ATTAQUE"<<endl<<endl<<endl<<endl
         <<"\t\t\t\t\t    Score J1 = ";textcolor(YELLOW);cout<<score1;textcolor(WHITE);cout<<"    Score J2 = ";textcolor(YELLOW);cout<<score2;

     plateau(80,7);
     affiche_grille_att(g_ia);







    }*/

/******** MAIN ********/

int main ()
   {
    int score1,score2;
    bool ia,abandon;

    do
      {
       do
         {
          abandon=false;
          ia=menu(score1,score2,abandon);
         }
       while(((score1!=17)&&(score2!=17))||(abandon==true));

       textbackground(BLACK);
       _setcursortype(_NOCURSOR);
       system("cls");
       textcolor(LIGHTRED);
       cout<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<"\t\t\t\t\t\t     FIN DE PARTIE "<<endl<<endl<<endl<<endl<<endl;
       textcolor(WHITE);
       if(score1==17)
         {
          cout<<"\t\t     Le Joueur 1 gagne avec un score de ";textcolor(LIGHTMAGENTA);cout<<score1;textcolor(WHITE);cout<<" contre l";
          if(ia==false)
            {
             cout<<"e Joueur 2 avec un score de ";textcolor(LIGHTMAGENTA);cout<<score2;
            }
          else
            {
             cout<<"\x27IA avec un score de ";textcolor(LIGHTMAGENTA);cout<<score2;
            }
         }
       else
         {
          cout<<"\t\t     L";
          if(ia==false)
            {
             cout<<"e Joueur 2 gagne avec un score de ";textcolor(LIGHTMAGENTA);cout<<score2;textcolor(WHITE);cout<<" contre le Joueur 1 avec un score de ";textcolor(LIGHTMAGENTA);cout<<score1;
            }
          else
            {
             cout<<"\x27IA gagne avec un score de ";textcolor(LIGHTMAGENTA);cout<<score2;textcolor(WHITE);cout<<" contre le Joueur 1 avec un score de ";textcolor(LIGHTMAGENTA);cout<<score1;
            }
         }

       textcolor(LIGHTGREEN);
       cout<<endl<<endl<<endl<<endl<<"\t\t\t\t      Tapez sur une touche pour revenir au menu ";

       waitkeypressed();
      }
    while(1);

    return (0);
   }
